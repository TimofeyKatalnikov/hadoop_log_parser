import sys

import httpagentparser


def mapper(line, separator='\t'):
    """
    Mapper for finding browser in web-server logs' string

    :param line: string from web-server logs
    :type line: str
    :param separator: Divider between key and value
    :type separator: str
    """
    line = line.strip()

    try:
        user_agent = httpagentparser.simple_detect(line)[1].split()
    except Exception:
        return

    user_agent_raw = []
    for word in user_agent:
        if word[0].isalpha() and word[-1].isalpha():
            user_agent_raw.append(word)
        else:
            break

    user_agent_raw = ' '.join(user_agent_raw)
    if user_agent_raw:
        print('{}{}{}'.format(user_agent_raw, separator, 1))


if __name__ == '__main__':
    for line in sys.stdin:
        mapper(line)
