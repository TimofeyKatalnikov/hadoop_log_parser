import sys
from itertools import groupby
from operator import itemgetter


def read_mapper_output(stdin, separator='\t'):
    """
    Read data from mapper output

    :param stdin: Standard input
    :param separator: Divider between key and value
    :type separator: str
    :return: [<key>, '1']
    :rtype: generator [str, str]
    """
    for line in stdin:
        yield line.rstrip().split(separator, 1)


def reducer(separator='\t'):
    """
    Reducer for aggregation browsers by name

    :param separator: Divider between key and value
    :type separator: str
    """
    data = read_mapper_output(sys.stdin, separator=separator)

    for current_word, group in groupby(data, itemgetter(0)):
        try:
            total_count = sum(int(count) for current_word, count in group)
            print('{}{}{}'.format(current_word, separator, total_count))
        except ValueError:
            pass


if __name__ == "__main__":
    reducer()
