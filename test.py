import unittest
import subprocess


LOG1 = '109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"'
LOG2 = '52.192.135.95 - - [13/Dec/2015:16:48:57 +0100] "GET / HTTP/1.1" 200 10479 "-" "Mozilla/4.0 (compatible; MSIE8.0; Windows NT 6.0) .NET CLR 2.0.50727)" "-"'
LOG3 = '37.1.206.196 - - [13/Dec/2015:17:16:04 +0100] "GET /administrator/index.php HTTP/1.1" 200 4263 "http://almhuette-raith.at/administrator/index.php" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36" "-"'
LOG4 = '188.23.50.138 - - [13/Dec/2015:18:29:16 +0100] "GET /images/stories/slideshow/almhuette_raith_04.jpg HTTP/1.1" 200 80637 "http://www.almhuette-raith.at/" "Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-T530 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.2 Chrome/38.0.2125.102 Safari/537.36" "-"'


class MRTest(unittest.TestCase):

    input = '{}\n{}\n{}\n{}'.format(LOG1, LOG2, LOG3, LOG4)

    def test_mapper(self):
        expected = 'Firefox\t1\n' \
                   'Microsoft Internet Explorer\t1\n' \
                   'Chrome\t1\n' \
                   'Chrome\t1\n'

        result = subprocess.check_output(
            "echo '{}' | python mapper.py".format(self.input), shell=True
        )
        self.assertEqual(expected, result.decode())

    def test_reducer(self):
        expected = 'Chrome\t2\n' \
                   'Firefox\t1\n' \
                   'Microsoft Internet Explorer\t1\n'

        result = subprocess.check_output(
            "echo '{}' | python mapper.py | sort -k1,1 | python reducer.py"
            .format(self.input), shell=True
        )
        self.assertEqual(expected, result.decode())


if __name__ == '__main__':
    unittest.main()
